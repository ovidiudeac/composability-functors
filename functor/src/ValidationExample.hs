module ValidationExample where

import Data.Monoid ((<>))
import Data.Char
import System.IO
import Text.Printf

newtype Error = Error String deriving (Show)
type Result a = Either Error a

newtype Name = Name String  deriving (Show)
newtype Address = Address String  deriving (Show)
data Person = Person { name :: Name, address :: Address}
  deriving (Show)

validateLength :: String -> Int -> String -> Either Error String
validateLength description n s =
  if len > n
    then Left $ Error $ "Too long " <> description
    else if len == 0
          then Left $ Error $ "Empty " <> description
          else Right s
  where len = length s

parseName :: String -> Result Name
parseName s = Name <$> validateLength "name" 20 s

parseAddress :: String -> Result Address
parseAddress s = Address <$> validateLength "address" 30 s

mkPerson :: String -> String -> Result Person
mkPerson n a = Person <$> parseName n <*> parseAddress a

readPerson :: IO (Result Person)
readPerson = do
  printf "Name: "
  hFlush stdout
  n <- getLine
  printf "Address: "
  hFlush stdout
  a <- getLine
  return $ mkPerson n a

example :: IO ()
example = do
  printf "Enter person data\n"
  person <- readPerson
  either
    (printf "Error: %s\n" . show)
    (printf "Person: %s\n" . show)
    person
