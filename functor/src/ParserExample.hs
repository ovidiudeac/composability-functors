{-# LANGUAGE FlexibleContexts #-}

module ParserExample where


import System.IO
import Text.Printf
import Text.Parsec
import Text.Parsec.String
import Data.Maybe (fromJust)

data Ast
    = Number Int
    | Identifier String
    | Parens Ast
    | Call String [Ast]
    deriving (Show, Eq)

data BinOp = Plus | Minus
    deriving (Show, Eq, Enum)

space1 = many1 space
name = many1 letter

number = Number <$> read <$> many1 digit

identifier = Identifier <$> name

parens = Parens <$ char '(' <*> body <* char ')'
  where body = number <|> identifier <|> call


call = Call <$> name <* space1 <*> args
  where args = sepBy arg space1
        arg = parens <|> number <|> identifier

ast :: Parser Ast
ast = parens <|> call <|> number <|> identifier

line = spaces *> ast <* spaces <* eof

example = do
  putStrLn "Enter string: "
  hFlush stdout
  s <- getLine
  putStrLn "Result:"
  print $ parse line "Cannot parse input" s
