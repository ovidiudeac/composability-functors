module SyntaxWalkthrough where

--
-- (.) :: (b -> c) -> (a -> b) -> (a -> c)
-- f . g = \ x -> f ( g x)
--
-- fmap :: (a -> b) -> f a -> f b
-- 
-- fmap . fmap = \ x -> fmap (fmap x)

-- Values
v :: Int
v = 10

myList :: [Int]
myList = [1,2,3]

myTuple :: (Int, String)
myTuple = (1,"abc")

myOption :: Maybe String
myOption = Nothing

myOtherOption :: Maybe Int
myOtherOption = Just 10

-- Functions
length :: String -> Int
length =  undefined

whatever :: a -> String
whatever = undefined

singleton :: a -> [a]
singleton = undefined

tuple :: a -> b -> (a,b)
tuple = undefined

curry :: ((a,b) -> c) -> a -> b -> c
curry = undefined

uncurry :: (a -> b -> c) -> (a,b) -> c
uncurry = undefined

-- Pattern matching

getOrDefault :: Maybe a -> a -> a
getOrDefault mv d =
  case mv of
    Just v -> v
    Nothing -> d

getOrDefault' (Just v) _ = v
getOrDefault' Nothing d = d

-- More functions
intToStr :: Int -> String
intToStr a = show a


s = intToStr 10
--
-- infixr 0 $
-- ($) :: (a -> b) -> a -> b
-- f $ a = f a

s3 = intToStr $ 10
