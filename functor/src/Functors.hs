module Functors where

-- Tree
data Tree a =
  Empty | Node a (Tree a) (Tree a)
  deriving (Show)

instance Functor Tree where
  fmap f Empty = Empty
  fmap f (Node v l r) = Node (f v) (f <$> l) (f <$> r)

data Maybe' a = Nothing' | Just' a
instance Functor Maybe' where
  fmap f Nothing' = Nothing'
  fmap f (Just' a) = Just' (f a)

data Either' e a = Left' e | Right' a
instance Functor (Either' e) where
  fmap f (Left' err) = Left' err
  fmap f (Right' v) = Right' (f v)


newtype Parser a = Parser (String -> Maybe a)
instance Functor Parser where
  fmap f (Parser p) = Parser (fmap f . p)
  -- fmap f (Parser p) = Parser p'
  --   where p' s = fmap f (p s)



newtype Transaction d a = Transaction (d -> Maybe (d, a))

instance Functor (Transaction d) where
  fmap f (Transaction tr) = Transaction ((fmap . fmap) f . tr )

data Contra a = Contra (a -> Int)
data Invariant a = Invariant (a->Int) (Int -> a)

data BadFunctor a = BadFunctor Int a
  deriving (Show, Eq)

instance Functor BadFunctor where
  fmap f (BadFunctor count v) = BadFunctor (count + 1) (f v)
