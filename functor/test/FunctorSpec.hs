{-# LANGUAGE ViewPatterns #-}
module FunctorSpec where

import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Function
import Test.QuickCheck.Gen

import Functors

type IdProperty f a = f a -> Bool
functorIdProp :: (Functor f, Eq (f a)) => IdProperty f a
-- fmap . id == id
-- \x -> fmap id x == id x
functorIdProp x = (fmap id x) == id x

type CompositionProperty f a b c = f a -> Fun a b -> Fun b c -> Bool
functorCompProp :: (Functor f, Eq (f c)) => CompositionProperty f a b c
functorCompProp x (apply -> f) (apply -> g) = (fmap (g . f) x) == (fmap g . fmap f $ x)

instance (Arbitrary a) => Arbitrary (BadFunctor a) where
    arbitrary = do
      NonNegative count <- arbitrary
      x <- arbitrary
      return $ BadFunctor count x

spec = do
  describe "Functor properties for (Either String)" $ do
    it "IdProperty (Either String) Int" $ do
      property (functorIdProp :: IdProperty (Either String) Int)

    it "CompositionProperty (Either String) Int String Float" $ do
      property (functorCompProp :: CompositionProperty (Either String) Int String Float)


    it "IdProperty (Either String) Double" $ do
      property (functorIdProp :: IdProperty (Either String) Double)
    it "CompositionProperty (Either String) Double String Float" $ do
      property (functorCompProp :: CompositionProperty (Either String) Double String Float)
  -- 
  -- describe "Functor properties for BadFunctor" $ do
  --   it "IdProperty BadFunctor Int" $ do
  --     property (functorIdProp :: IdProperty BadFunctor Int)
  --   it "CompositionProperty BadFunctor Int Int Int" $ do
  --     property (functorCompProp :: CompositionProperty BadFunctor Int Int Int)
